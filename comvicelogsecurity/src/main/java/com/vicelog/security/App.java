package com.vicelog.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Properties;

@SpringBootApplication
@EnableWebMvc
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class App implements WebMvcConfigurer {

    public static void main(String... args) {
        SpringApplication application = new SpringApplication(App.class);

        Properties properties = new Properties();
        properties.put("spring.jpa.properties.javax.persistence.validation.mode", "none");
        properties.put("server.port", "8080");
        properties.put("sessions.security.matchers", new String[]{"/api/**", "/session/security/token"});
        properties.put("sessions.security.authEndpointsEnabled", true);
        properties.put("sessions.security.enabled", true);
        properties.put("sessions.security.tokenKey", "Bearer");
        properties.put("sessions.security.tokenLifeTime", 30);
        application.setDefaultProperties(properties);

        application.run(args);
    }

}
