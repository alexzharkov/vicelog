package com.vicelog.security.service;

import com.vicelog.security.model.Session;

public interface SessionService {

    Session addSession();

    boolean isSessionActive(String token);

    Session revokeSession(String token);

    boolean dismissSession(String token);

}
