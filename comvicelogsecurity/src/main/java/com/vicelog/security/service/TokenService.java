package com.vicelog.security.service;

public interface TokenService {

    String requestToken();

}
