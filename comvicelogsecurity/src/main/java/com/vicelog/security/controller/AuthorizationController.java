package com.vicelog.security.controller;

import com.vicelog.security.model.Session;
import com.vicelog.security.service.SessionService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/session/security")
public class AuthorizationController {

    private final SessionService sessionService;

    @Autowired
    public AuthorizationController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @PostMapping()
    public ResponseEntity<Session> createSession() {
        return ResponseEntity.ok(sessionService.addSession());
    }

    @PatchMapping(value = "/token")
    public ResponseEntity revokeSession(@RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(sessionService.revokeSession(token.split(StringUtils.SPACE)[1]));
    }

    @DeleteMapping(value = "/token")
    public ResponseEntity dismissSession(@RequestHeader("Authorization") String token) {
        return sessionService.dismissSession(token.split(StringUtils.SPACE)[1])
                ? ResponseEntity.noContent().build()
                : ResponseEntity.badRequest().build();
    }

}
